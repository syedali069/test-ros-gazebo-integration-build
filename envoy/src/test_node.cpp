#include <ros/ros.h>

int main(int argc, char **argv)
{
  // Set up ROS.
  ros::init(argc, argv, "test_node");

  while (ros::ok())
  {
    ROS_INFO("Hello world!");

    ros::spinOnce();
  }
}
